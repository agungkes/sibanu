type RootStackParamList = {
  AUTH_HOME: undefined;
  LOGIN: undefined;
  REGISTER: undefined;

  HOME: undefined;
  CLASSROOM_LIST: {
    name: string;
  };
  CLASSROOM: {
    name: string;
  };

  STATION: {
    stationId: number;
    title: string;
    subtitle?: string;
  };
};

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}
