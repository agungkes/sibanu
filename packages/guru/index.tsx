import 'react-native-screens';
import { enableFreeze } from 'react-native-screens';
enableFreeze(true);

import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './src/screens/HomeScreen';
import AuthScreen from './src/screens/AuthScreen';

import {
  FONT_SEMIBOLD,
  theme,
  ThemeProvider as RNEProvider,
  width,
  Icon,
} from '@sibanu/app';

import RegisterScreen from '@guru/screens/AuthScreen/RegisterScreen';
import LoginScreen from '@guru/screens/AuthScreen/LoginScreen';

import ClassroomListScreen from '@guru/screens/Classroom/ClassroomListScreen';
import ClassroomDetailScreen from '@guru/screens/Classroom/ClassroomDetailScreen';
import StationScreen from '@guru/screens/Classroom/StationScreen';

const Stack = createNativeStackNavigator<RootStackParamList>();

const Navigator = () => {
  return (
    <RNEProvider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="HOME"
          screenOptions={{
            headerTintColor: theme.colors?.primary,
            headerTitleAlign: 'center',
            headerTitleStyle: {
              color: '#000',
              fontSize: width(18),
              fontFamily: FONT_SEMIBOLD,
            },
            headerStyle: {
              backgroundColor: '#f2f2f2',
            },
            headerShadowVisible: false,
            contentStyle: {
              padding: width(20),
              backgroundColor: '#f2f2f2',
            },
          }}>
          {/* Authentication */}
          <Stack.Group>
            <Stack.Screen
              name="AUTH_HOME"
              component={AuthScreen}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="REGISTER"
              component={RegisterScreen}
              options={{
                title: 'Register',
              }}
            />
            <Stack.Screen
              name="LOGIN"
              component={LoginScreen}
              options={{
                title: 'Sign In',
              }}
            />
          </Stack.Group>

          <Stack.Group>
            <Stack.Screen
              name="HOME"
              component={HomeScreen}
              options={{
                headerShown: false,
              }}
            />

            {/* Classroom */}
            <Stack.Screen
              name="CLASSROOM_LIST"
              component={ClassroomListScreen}
              options={({ route }) => ({
                title: route.params.name,
              })}
            />
            <Stack.Screen
              name="CLASSROOM"
              component={ClassroomDetailScreen}
              options={({ route }) => ({
                title: route.params.name,
                headerRight: () => (
                  <Icon
                    name="person-outline"
                    type="ionicon"
                    size={width(16)}
                    color={theme.colors?.primary}
                  />
                ),
              })}
            />
            <Stack.Screen
              name="STATION"
              component={StationScreen}
              options={({ route }) => ({
                title: route.params.title,
                headerRight: () => (
                  <Icon
                    name="reader-outline"
                    type="ionicon"
                    size={width(16)}
                    color={theme.colors?.primary}
                  />
                ),
              })}
            />
          </Stack.Group>
        </Stack.Navigator>
      </NavigationContainer>
    </RNEProvider>
  );
};

AppRegistry.registerComponent(appName, () => Navigator);
