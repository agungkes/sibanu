import { HeaderName, ClassList } from '@sibanu/app';
import { NativeStackScreenProps } from 'packages/murid/node_modules/@react-navigation/native-stack/lib/typescript/src';
import React from 'react';
import { ScrollView } from 'react-native';

import styles from './HomeScreen.styles';

const CLASS_SOURCE = [
  {
    title: 'Tema 1',
    count: '4',
  },
  {
    title: 'Tema 2',
    count: '5',
  },
];

type Props = NativeStackScreenProps<RootStackParamList, 'LOGIN'>;

const HomeScreen = ({ navigation }: Props) => {
  const handleGoToClass = () => {
    navigation.navigate('CLASSROOM_LIST', {
      name: 'Bahasa Indonesia',
    });
  };
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <HeaderName name="Agung Kurniawan Eka S" school="SMPN 2 Banguntapan" />
      <ClassList
        title="Tematik"
        dataSource={CLASS_SOURCE}
        onListPress={handleGoToClass}
      />
    </ScrollView>
  );
};

export default HomeScreen;
