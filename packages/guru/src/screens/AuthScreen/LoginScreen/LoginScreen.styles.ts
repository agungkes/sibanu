import { theme, width } from '@sibanu/app';

import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  bottomSheetContainer: {
    backgroundColor: theme.colors?.white,
    padding: width(16),
  },
});

export default styles;
