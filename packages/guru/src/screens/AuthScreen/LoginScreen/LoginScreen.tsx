import React, { useState } from 'react';
import { Signin, BottomSheet, Input, Button } from '@sibanu/app';
import { View } from 'react-native';
import styles from './LoginScreen.styles';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

type Props = NativeStackScreenProps<RootStackParamList, 'LOGIN'>;

const LoginScreen = ({ navigation }: Props) => {
  const [isLoginSuccess, setLoginSuccess] = useState(false);

  const handleGoToRegister = () => {
    navigation.navigate('REGISTER');
  };
  return (
    <React.Fragment>
      <Signin onPressRegister={handleGoToRegister} />
      <BottomSheet isVisible={isLoginSuccess}>
        <View style={styles.bottomSheetContainer}>
          <Input placeholder="Masukkan kode kelas" label="Kode Kelas" />
          <Button title="Masuk Kelas" />
        </View>
      </BottomSheet>
    </React.Fragment>
  );
};

export default LoginScreen;
