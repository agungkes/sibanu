import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { BottomSheet, Button, Icon } from '@sibanu/app';
import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import DocumentPicker from 'react-native-document-picker';
type Props = NativeStackScreenProps<RootStackParamList, 'STATION'>;
const StationScreen: React.FC<Props> = ({
  navigation,
  route: { params },
}): JSX.Element => {
  const [file, setFile] = React.useState('');
  const [showOption, setShowOption] = React.useState(false);

  const handleToggleOption = () => setShowOption(prev => !prev);
  const handleOpenPicker = (fileType: any) => () => {
    DocumentPicker.pick({
      type: fileType,
    });
  };

  return (
    <React.Fragment>
      <ScrollView>
        <View
          style={{
            padding: 20,
            backgroundColor: '#fff',
            borderRadius: 10,
          }}>
          <Text>
            Putarlah rekaman berikut ini untuk mendengarkan petunjuk pengerjaan!
          </Text>
        </View>

        <View>
          <Text>Upload Audio</Text>
          <Text>
            Anda dapat mengunggah materi/ instruksi pembelajaran dalam bentuk
            audio atau audio visual
          </Text>
          <Button title={'Upload Audio'} onPress={handleToggleOption} />
        </View>
        <View>
          <Text>Upload Dokumen</Text>
          <Text>
            Anda dapat mengunggah materi/ instruksi pembelajaran dalam bentuk
            pdf, ppt, atau jpg
          </Text>
          <Button title={'Upload Dokumen'} onPress={handleToggleOption} />
        </View>

        <Button title="Unggah Tugas" type="outline" />
      </ScrollView>

      <BottomSheet
        isVisible={showOption}
        modalProps={{
          onRequestClose: () => {
            setShowOption(false);
          },
        }}>
        <TouchableWithoutFeedback onPress={() => setShowOption(false)}>
          <View>
            <TouchableWithoutFeedback>
              <View
                style={{
                  borderTopLeftRadius: 10,
                  borderTopRightRadius: 10,
                  backgroundColor: '#F2F2F2',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 40,
                }}>
                <Text
                  style={{
                    marginBottom: 24,
                    fontSize: 14,
                  }}>
                  Pilih file melalui
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-around',
                  }}>
                  <View
                    style={{
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={handleOpenPicker(DocumentPicker.types.audio)}
                      style={{
                        backgroundColor: '#fff',
                        padding: 10,
                        borderRadius: 50,
                      }}>
                      <Icon
                        name="mic-outline"
                        type="ionicon"
                        size={32}
                        color="#FE502F"
                      />
                    </TouchableOpacity>

                    <Text
                      style={{
                        fontSize: 14,
                        marginTop: 8,
                      }}>
                      Audio
                    </Text>
                  </View>

                  <View
                    style={{
                      alignItems: 'center',
                      marginLeft: 36,
                    }}>
                    <TouchableOpacity
                      onPress={handleOpenPicker(DocumentPicker.types.video)}
                      style={{
                        backgroundColor: '#fff',
                        padding: 10,
                        borderRadius: 50,
                      }}>
                      <Icon
                        name="videocam-outline"
                        type="ionicon"
                        size={32}
                        color="#FE502F"
                      />
                    </TouchableOpacity>

                    <Text
                      style={{
                        fontSize: 14,
                        marginTop: 8,
                      }}>
                      Video
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </BottomSheet>
    </React.Fragment>
  );
};

export default StationScreen;
