import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RoomList } from '@sibanu/app';
import React from 'react';

const DATA_SOURCE = [
  {
    id: 1,
    name: 'Kelas 1 A',
    deskripsi: 'Lorem ipsum dolor sit amet',
    link: 'https://google.co.id',
    members: [
      {
        id: 1,
        name: 'Agung Kurniawan',
      },
      {
        id: 2,
        name: 'Nindia Pratiwi',
      },
      {
        id: 3,
        name: 'Ghozia Ulhaq',
      },
      {
        id: 4,
        name: 'Annisa Eka Hapsari',
      },
    ],
  },
];
type Props = NativeStackScreenProps<RootStackParamList, 'CLASSROOM_LIST'>;
const ClassroomListScreen = ({ navigation }: Props) => {
  const handleGoToClass = (item: typeof DATA_SOURCE[0]) => {
    navigation.navigate('CLASSROOM', {
      name: item.name,
    });
  };
  return <RoomList dataSource={DATA_SOURCE} onPress={handleGoToClass} />;
};

export default ClassroomListScreen;
