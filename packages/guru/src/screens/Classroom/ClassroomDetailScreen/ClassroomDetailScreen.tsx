import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { StationList } from '@sibanu/app';
import React from 'react';

const DATA_SOURCE = [
  {
    id: 1,
    title: 'Sumber Bacaan',
  },
  {
    id: 2,
    title: 'Menjelajahi Tema',
    subtitle: 'Station 1',
  },
];
type Props = NativeStackScreenProps<RootStackParamList, 'CLASSROOM'>;
const ClassroomDetailScreen = ({ navigation }: Props): JSX.Element => {
  const handleGoToStation = (item: typeof DATA_SOURCE[0]): void => {
    navigation.navigate('STATION', {
      stationId: 1,
      title: item.title,
      subtitle: item.subtitle,
    });
  };
  return <StationList dataSource={DATA_SOURCE} onPress={handleGoToStation} />;
};

export default ClassroomDetailScreen;
