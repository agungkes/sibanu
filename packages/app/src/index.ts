import theme from './theme';

export { theme };

export * from './components';
export * from './fonts';
export * from './helpers';
export * from 'react-native-elements';
