import { width, height } from '../../../helpers';
import { FONT_REGULAR } from '../../../fonts';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    marginBottom: height(10),
    borderRadius: width(10),
  },
  titleText: {
    fontSize: width(16),
    fontFamily: FONT_REGULAR,
  },
  subtitleText: {
    fontSize: width(14),
    fontFamily: FONT_REGULAR,
    color: '#5b5b5b',
  },
});

export default styles;
