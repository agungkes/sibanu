import { width } from '../../../helpers';
import React from 'react';
import { Avatar, ListItem } from 'react-native-elements';

import styles from './ClassListItem.styles';
type Props = {
  title: string;
  count: number;
  onListPress?: () => void;
};
const ClassListItem: React.FC<Props> = ({ title, count, onListPress }) => {
  return (
    <ListItem onPress={onListPress} containerStyle={styles.container}>
      <Avatar
        source={require('../../../assets/images/logo.png')}
        rounded
        size="medium"
      />
      <ListItem.Content>
        <ListItem.Title style={styles.titleText}>{title}</ListItem.Title>
        <ListItem.Subtitle style={styles.subtitleText}>
          {count} Kelas
        </ListItem.Subtitle>
      </ListItem.Content>

      <ListItem.Chevron
        size={width(24)}
        color="#333333"
        type="ionicon"
        name="chevron-forward"
      />
    </ListItem>
  );
};

export default ClassListItem;
