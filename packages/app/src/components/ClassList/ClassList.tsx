import React from 'react';
import { Text } from 'react-native-elements';
import ClassListItem from './ClassListItem';

import styles from './ClassList.styles';

type Props = {
  title: string;
  dataSource?: any[];
  onListPress?: () => void;
};
const ClassList = ({ dataSource, onListPress, title }: Props) => {
  return (
    <React.Fragment>
      <Text style={styles.titleText}>{title}</Text>
      {dataSource?.map(data => (
        <ClassListItem key={data.title} {...data} onListPress={onListPress} />
      ))}
    </React.Fragment>
  );
};

export default ClassList;
