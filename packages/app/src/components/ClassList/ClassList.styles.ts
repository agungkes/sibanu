import { FONT_MEDIUM } from '../../fonts';
import { width } from '../../helpers';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  titleText: {
    fontSize: width(16),
    fontFamily: FONT_MEDIUM,
    color: '#333333',
  },
});

export default styles;
