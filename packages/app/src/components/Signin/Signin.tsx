import React, { useState } from 'react';
import { ScrollView, View } from 'react-native';
import { Input, Icon, useTheme, Button, Text } from 'react-native-elements';
import styles from './Signin.styles';

type Props = {
  onLogin?: () => void;
  onLoginWithGoogle?: () => void;
  onPressRegister?: () => void;
  loading?: boolean;
};
const Signin: React.FC<Props> = ({
  onLogin,
  onLoginWithGoogle,
  onPressRegister,
  loading,
}) => {
  const { theme } = useTheme();
  const primaryColor = theme.colors?.primary;
  const [isShowPassword, setShowPassword] = useState(false);

  const handleTogglePasswordVisibility = () => {
    setShowPassword(prev => !prev);
  };

  return (
    <ScrollView
      keyboardShouldPersistTaps="always"
      contentContainerStyle={styles.container}>
      <View>
        <Input
          keyboardType="email-address"
          returnKeyType="next"
          textContentType="emailAddress"
          placeholder="email@sibanu.com"
          label="Alamat Surel"
          disabled={loading}
        />
        <Input
          returnKeyType="done"
          textContentType="password"
          secureTextEntry={!isShowPassword}
          placeholder="supersecretpassword123"
          label="Kata Sandi"
          disabled={loading}
          rightIcon={
            isShowPassword ? (
              <Icon
                name="eye-off"
                color={primaryColor}
                type="ionicon"
                onPress={handleTogglePasswordVisibility}
              />
            ) : (
              <Icon
                name="eye"
                color={primaryColor}
                type="ionicon"
                onPress={handleTogglePasswordVisibility}
              />
            )
          }
        />

        <View>
          <Button title="Masuk" onPress={onLogin} loading={loading} />
          <Text style={[styles.infoText, { marginVertical: 10, marginTop: 0 }]}>
            atau
          </Text>
          <Button
            type="outline"
            title="Lanjutkan dengan Google"
            iconPosition="left"
            onPress={onLoginWithGoogle}
            icon={{
              type: 'ionicon',
              name: 'logo-google',
              color: theme.colors?.primary,
              containerStyle: {
                marginRight: 10,
              },
            }}
          />
        </View>
        <Text style={styles.infoText}>
          Belum punya akun?{' '}
          <Text
            style={[styles.infoText, styles.infoTextBold]}
            onPress={onPressRegister}>
            Daftar
          </Text>
        </Text>
      </View>
    </ScrollView>
  );
};

export default Signin;
