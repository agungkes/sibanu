import React from 'react';
import { ImageSourcePropType, View } from 'react-native';
import { Avatar, Text } from 'react-native-elements';

import styles from './HeaderName.styles';

type Props = {
  name: string;
  school: string;
  avatarSource?: ImageSourcePropType;
};
const HeaderName: React.FC<Props> = ({ name, school, avatarSource }) => {
  const avatarTitle = name.split(' ');
  const title =
    avatarTitle.length === 1
      ? avatarTitle[0]
      : `${avatarTitle[0][0]}${avatarTitle[1][0]}`;

  return (
    <View style={styles.container}>
      <Avatar title={title} size="large" rounded source={avatarSource} />
      <Text style={styles.nameText}>{name}</Text>
      <Text style={styles.schoolText}>{school}</Text>
    </View>
  );
};

export default HeaderName;
