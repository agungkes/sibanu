import { width, height } from '../../helpers';
import { StyleSheet } from 'react-native';
import { FONT_SEMIBOLD, FONT_REGULAR } from '../../fonts';

const styles = StyleSheet.create({
  container: {
    padding: width(20),
    backgroundColor: '#3853A9',
    borderRadius: width(10),
    marginBottom: height(30),
  },
  nameText: {
    fontSize: width(16),
    fontFamily: FONT_SEMIBOLD,
    color: '#F2f2f2',
  },
  schoolText: {
    fontSize: width(16),
    fontFamily: FONT_REGULAR,
    color: '#b8b8b8',
    marginTop: height(8),
  },
});

export default styles;
