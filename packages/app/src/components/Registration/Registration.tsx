import React, { useState } from 'react';
import { ScrollView, View } from 'react-native';
import {
  Input,
  Icon,
  Button,
  Text,
  useTheme,
  CheckBox,
} from 'react-native-elements';

import styles from './Registration.style';

const Registration = () => {
  const { theme } = useTheme();
  const primaryColor = theme.colors?.primary;

  const [isShowPassword, setShowPassword] = useState(false);
  const [pendidik, setPendidik] = useState('SD');

  const handleTogglePasswordVisibility = () => {
    setShowPassword(prev => !prev);
  };
  const handleToggleTenagaPendidik = (selectedPendidik: string) => () => {
    setPendidik(selectedPendidik);
  };
  return (
    <ScrollView
      keyboardShouldPersistTaps="always"
      contentContainerStyle={styles.container}>
      <View>
        <Input
          keyboardType="email-address"
          returnKeyType="next"
          textContentType="emailAddress"
          placeholder="email@sibanu.com"
          label="Alamat Surel"
        />
        <Input
          returnKeyType="next"
          textContentType="name"
          placeholder="Nindia Pratiwi"
          label="Nama Lengkap"
        />
        <Input returnKeyType="next" placeholder="123456xxxx" label="NIP" />
        <Input
          returnKeyType="next"
          textContentType="telephoneNumber"
          placeholder="08123456789"
          label="Nomor Telepon"
        />

        <Input
          returnKeyType="done"
          textContentType="password"
          secureTextEntry={!isShowPassword}
          placeholder="supersecretpassword123"
          label="Kata Sandi"
          rightIcon={
            isShowPassword ? (
              <Icon
                name="eye-off"
                color={primaryColor}
                type="ionicon"
                onPress={handleTogglePasswordVisibility}
              />
            ) : (
              <Icon
                name="eye"
                color={primaryColor}
                type="ionicon"
                onPress={handleTogglePasswordVisibility}
              />
            )
          }
        />

        <View>
          <Text style={styles.labelStyle}>Tenaga Pendidik</Text>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <CheckBox
              center
              title="SD"
              iconType="ionicon"
              checkedIcon="radio-button-on"
              uncheckedIcon="radio-button-off"
              checked={pendidik === 'SD'}
              onPress={handleToggleTenagaPendidik('SD')}
            />
            <CheckBox
              center
              title="SMP"
              iconType="ionicon"
              checkedIcon="radio-button-on"
              uncheckedIcon="radio-button-off"
              checked={pendidik === 'SMP'}
              onPress={handleToggleTenagaPendidik('SMP')}
            />
          </View>
        </View>

        <Button title="Register" />
        <Text style={styles.infoText}>
          Sudah punya akun?{' '}
          <Text style={[styles.infoText, styles.infoTextBold]}>Masuk</Text>
        </Text>
      </View>
    </ScrollView>
  );
};

export default Registration;
