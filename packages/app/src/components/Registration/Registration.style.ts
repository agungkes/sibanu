import { StyleSheet } from 'react-native';
import { height, width } from '../../helpers';
import { FONT_SEMIBOLD, FONT_MEDIUM } from '../../fonts';

const style = StyleSheet.create({
  container: {
    padding: width(20),
  },
  infoText: {
    textAlign: 'center',
    fontSize: width(14),
    marginTop: height(58),
  },
  infoTextBold: {
    fontFamily: FONT_SEMIBOLD,
  },
  labelStyle: {
    fontSize: height(20),
    fontFamily: FONT_MEDIUM,
    marginBottom: height(16),
    color: '#333333',
  },
});

export default style;
