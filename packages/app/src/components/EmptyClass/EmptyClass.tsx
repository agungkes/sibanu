import React, { useState } from 'react';
import { View } from 'react-native';
import { BottomSheet, Button, Image, Input, Text } from 'react-native-elements';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import styles from './EmptyClass.styles';

import { object, string, SchemaOf } from 'yup';

type IFormData = {
  tingkatan_kelas: string;
  nama_kelas: string;
  kode_kelas: string;
  deskripsi?: string;
};
const schema: SchemaOf<IFormData> = object({
  tingkatan_kelas: string().required(),
  nama_kelas: string().required(),
  kode_kelas: string().required(),
  deskripsi: string().notRequired(),
}).required();

type Props = {
  onPressCreateClass?: (values: IFormData) => void;
};
const EmptyClass: React.FC<Props> = ({ onPressCreateClass }) => {
  const [isShowCreate, setShowCreate] = useState(false);

  const {
    control,
    formState: { isSubmitting, errors },
    handleSubmit,
    reset,
  } = useForm<IFormData>({
    resolver: yupResolver(schema),
  });

  const handleToggleCreate = () => {
    setShowCreate(prev => !prev);
  };

  const handlePressCreate = (values: IFormData) => {
    if (onPressCreateClass) {
      onPressCreateClass(values);
    }
    reset();
    handleToggleCreate();
  };
  return (
    <React.Fragment>
      <View style={styles.container}>
        <Image
          source={require('../../assets/images/empty-class.png')}
          style={styles.emptyImage}
          resizeMode="contain"
        />
        <Text style={styles.emptyText}>
          Masih belum ada kelas. Mari kita mulai dengan membuat kelas dan
          kemudian memulai pembelajaran di kelas!
        </Text>
        <Button title={'Buat Kelas'} onPress={handleToggleCreate} />
      </View>

      <BottomSheet
        isVisible={isShowCreate}
        scrollViewProps={{
          keyboardShouldPersistTaps: 'always',
        }}
        modalProps={{
          hardwareAccelerated: true,
        }}>
        <View style={styles.bottomSheetContainer}>
          <Controller
            control={control}
            name="tingkatan_kelas"
            render={({ field: { onChange, onBlur, value } }) => (
              <Input
                label="Tingkat Kelas"
                placeholder="Pilih tingkatan kelas"
                returnKeyType="next"
                rightIcon={{
                  name: 'arrow-down',
                  type: 'ionicon',
                }}
                disabled={isSubmitting}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
                errorMessage={errors.tingkatan_kelas?.message}
              />
            )}
          />
          <Controller
            name="nama_kelas"
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <Input
                label="Nama Kelas"
                placeholder="Nama Kelas"
                returnKeyType="next"
                disabled={isSubmitting}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
                errorMessage={errors?.nama_kelas?.message}
              />
            )}
          />

          <Controller
            name="kode_kelas"
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <Input
                label="Kode Kelas"
                placeholder="Tulis kode kelas"
                returnKeyType="next"
                disabled={isSubmitting}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
                errorMessage={errors?.kode_kelas?.message}
              />
            )}
          />

          <Controller
            name="deskripsi"
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <Input
                label="Deskripsi"
                placeholder="Tulis deskripsi"
                returnKeyType="next"
                disabled={isSubmitting}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
                errorMessage={errors?.deskripsi?.message}
              />
            )}
          />

          <Button
            title="Buat Kelas"
            onPress={handleSubmit(handlePressCreate)}
            loading={isSubmitting}
          />
          <Button
            type="outline"
            title="Batal"
            onPress={handleToggleCreate}
            disabled={isSubmitting}
          />
        </View>
      </BottomSheet>
    </React.Fragment>
  );
};

export default EmptyClass;
