import { width, height } from '../../helpers';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    height: '100%',
  },
  emptyText: {
    fontSize: width(16),
    color: '#5b5b5b',
    textAlign: 'center',
    marginTop: height(30),
    marginBottom: height(190),
  },
  emptyImage: {
    width: '100%',
    height: height(220),
  },

  bottomSheetContainer: {
    backgroundColor: '#f2f2f2',
    padding: width(20),
  },
});

export default styles;
