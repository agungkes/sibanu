import { StyleSheet } from 'react-native';
import { width } from '../../helpers';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    borderColor: '#fff',
    borderWidth: width(3),
    // backgroundColor: 'red',
  },
  avatarTitle: {
    fontSize: width(12),
  },
  avatarContainer: {
    marginLeft: -width(15),
  },
});

export default styles;
