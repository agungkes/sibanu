import { generateTitle, generateColor } from '../../helpers';
import React from 'react';
import { ImageSourcePropType, View } from 'react-native';
import { Avatar, Text } from 'react-native-elements';
import styles from './Member.styles';

type Props = {
  data?: {
    id: number;
    avatar?: ImageSourcePropType;
    name: string;
  }[];
};
const Member: React.FC<Props> = ({ data }) => {
  return (
    <View style={styles.container}>
      {data?.slice(0, 3).map((member, idx: number) => (
        <Avatar
          key={member.id}
          title={generateTitle(member.name)}
          titleStyle={styles.avatarTitle}
          source={member.avatar}
          size="small"
          rounded
          avatarStyle={styles.avatar}
          containerStyle={[
            { backgroundColor: generateColor(member.name) },
            idx !== 0 && styles.avatarContainer,
          ]}
        />
      ))}

      {(data?.length || 0) > 3 && <Text>+{(data?.length || 0) - 3}</Text>}
    </View>
  );
};

export default Member;
