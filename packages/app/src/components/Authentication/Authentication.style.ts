import { StyleSheet } from 'react-native';
import { height, width } from '../../helpers';
import { FONT_SEMIBOLD } from '../../fonts';

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    padding: width(20),
  },
  agreementText: {
    textAlign: 'center',
    fontSize: height(16),
  },
  agreementTextBold: {
    fontFamily: FONT_SEMIBOLD,
  },
  titleText: {
    fontFamily: FONT_SEMIBOLD,
    fontSize: height(30),
    marginBottom: height(50),
    textAlign: 'center',
  },
  logo: {
    width: '100%',
    height: height(150),
  },
  logoContainer: {
    alignItems: 'center',
    alignContent: 'center',
    marginBottom: height(150),
  },
});

export default style;
