import React from 'react';
import { ScrollView, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';

import styles from './Authentication.style';

type Props = {
  title: string;
  onRegister?: () => void;
  onSignIn?: () => void;
};
const Authentication: React.FC<Props> = ({
  title = 'pendidik',
  onRegister,
  onSignIn,
}) => {
  return (
    <ScrollView
      keyboardShouldPersistTaps="always"
      contentContainerStyle={styles.container}>
      <View>
        <Image
          source={require('../../assets/images/logo.png')}
          style={styles.logo}
          containerStyle={styles.logoContainer}
          resizeMode="contain"
        />
        <Text style={styles.titleText}>Selamat datang {title} di SiBaNu!</Text>
        <Button title={'Daftar'} onPress={onRegister} />
        <Button type="outline" title={'Masuk'} onPress={onSignIn} />

        <Text style={styles.agreementText}>
          By signing up you accept the{' '}
          <Text style={[styles.agreementText, styles.agreementTextBold]}>
            Term of Service and Privacy Policy
          </Text>
        </Text>
      </View>
    </ScrollView>
  );
};

export default Authentication;
