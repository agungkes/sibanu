import React from 'react';
import { FlatList } from 'react-native';
import { Avatar, ListItem } from 'react-native-elements';

import styles from './StationList.styles';

type DataSource = {
  id: number;
  title: string;
  subtitle?: string;
};
type Props = {
  dataSource?: DataSource[];
  onPress?: (item: DataSource) => void;
};
const StationList: React.FC<Props> = ({ dataSource, onPress }): JSX.Element => {
  const handlePress = (item: DataSource) => () => {
    if (onPress) {
      onPress(item);
    }
  };
  const keyExtractor = (item: DataSource) => `${item.id}`;
  const renderItem = ({ item }: { item: DataSource }) => {
    return (
      <ListItem
        containerStyle={styles.listItemContainer}
        onPress={handlePress(item)}>
        <Avatar title="SB" size="large" avatarStyle={styles.listItemAvatar} />
        <ListItem.Content>
          {item.subtitle && (
            <ListItem.Title style={styles.listItemSubtitle}>
              {item.subtitle}
            </ListItem.Title>
          )}
          <ListItem.Title style={styles.listItemTitle}>
            {item.title}
          </ListItem.Title>
        </ListItem.Content>
      </ListItem>
    );
  };

  return (
    <React.Fragment>
      <FlatList
        data={dataSource}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </React.Fragment>
  );
};

export default StationList;
