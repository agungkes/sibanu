import { width, height } from '../../helpers';
import { StyleSheet } from 'react-native';
import { FONT_SEMIBOLD } from '../../fonts';

const styles = StyleSheet.create({
  listItemContainer: {
    backgroundColor: '#fff',
    borderRadius: width(20),
    marginBottom: height(16),
  },
  listItemAvatar: {
    backgroundColor: '#fab',
    borderRadius: width(10),
  },
  listItemTitle: {
    fontSize: width(16),
    fontFamily: FONT_SEMIBOLD,
    color: '#333333',
  },
  listItemSubtitle: {
    fontSize: width(14),
    fontFamily: FONT_SEMIBOLD,
    color: '#5B5B5B',
  },
});

export default styles;
