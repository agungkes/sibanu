import { width, height } from '../../helpers';
import { Dimensions, StyleSheet } from 'react-native';
import theme from '../../theme';

const screenHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderRadius: width(10),
  },

  contentTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginBottom: height(16),
  },
  badge: {
    paddingVertical: height(7),
    paddingHorizontal: width(15),
    backgroundColor: '#ffeae6',
    borderRadius: width(10),
  },
  badgeText: {
    color: theme.colors?.primary,
    fontSize: width(12),
  },

  titleText: {
    fontSize: width(18),
    color: '#333333',
  },

  contentBottom: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  descriptionText: {
    fontSize: width(14),
    color: '#bbbbbb',
  },

  bottomSheet: {
    flex: 1,
    height: screenHeight,
    justifyContent: 'flex-end',
    paddingBottom: 25,
  },
});

export default styles;
