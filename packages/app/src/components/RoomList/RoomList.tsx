import EmptyClass from '../EmptyClass';
import React, { useState } from 'react';
import {
  FlatList,
  TouchableWithoutFeedback,
  View,
  ImageSourcePropType,
} from 'react-native';
import {
  BottomSheet,
  Icon,
  ListItem,
  Text,
  useTheme,
} from 'react-native-elements';
import Clipboard from '@react-native-clipboard/clipboard';

import styles from './RoomList.styles';
import Member from '../Member';

type Member = { id: number; avatar?: ImageSourcePropType; name: string };
type DataSource = {
  id: number;
  name: string;
  deskripsi: string;
  link: string;
  members: Member[];
};

type RoomListProps = {
  dataSource?: DataSource[];
  onPress?: (item: DataSource) => void;
};
const RoomList: React.FC<RoomListProps> = ({ dataSource = [], onPress }) => {
  const { theme } = useTheme();
  const [showOption, setShowOption] = useState(false);
  const [selectedRoom, setSelectedRoom] = useState('');

  const handleToogleShowOption = (i?: string) => () => {
    if (i) {
      setSelectedRoom(i);
    }
    setShowOption(prev => !prev);
  };

  const handleCopy = () => {
    Clipboard.setString(selectedRoom);
    setShowOption(false);
  };

  const handlePress = (item: DataSource) => () => {
    if (onPress) {
      onPress(item);
    }
  };
  const keyExtractor = (item: DataSource) => `${item.id}`;
  const renderItem = ({ item }: { item: DataSource }) => {
    return (
      <ListItem containerStyle={styles.container} onPress={handlePress(item)}>
        <ListItem.Content>
          <View style={styles.contentTop}>
            <View style={styles.badge}>
              <Text style={styles.badgeText}>Kelas I</Text>
            </View>

            <Icon
              type="ionicon"
              name="ellipsis-vertical"
              color={theme.colors?.primary}
              onPress={handleToogleShowOption(item.link)}
            />
          </View>

          <ListItem.Title style={styles.titleText}>{item.name}</ListItem.Title>

          <View style={styles.contentBottom}>
            <Text style={styles.descriptionText}>{item.deskripsi}</Text>
            <Member data={item.members} />
          </View>
        </ListItem.Content>
      </ListItem>
    );
  };
  return (
    <React.Fragment>
      <FlatList
        ListEmptyComponent={<EmptyClass />}
        contentContainerStyle={{
          flex: 1,
        }}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        data={dataSource}
      />

      <BottomSheet
        isVisible={showOption}
        modalProps={{
          onRequestClose: () => {
            setShowOption(false);
          },
        }}>
        <TouchableWithoutFeedback onPress={() => setShowOption(false)}>
          <View style={styles.bottomSheet}>
            <TouchableWithoutFeedback>
              <View>
                <ListItem onPress={handleCopy}>
                  <ListItem.Content>
                    <ListItem.Title>Salin link kelas</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
                <ListItem>
                  <ListItem.Content>
                    <ListItem.Title>Edit kelas</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
                <ListItem>
                  <ListItem.Content>
                    <ListItem.Title>Hapus kelas</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </BottomSheet>
    </React.Fragment>
  );
};

export default RoomList;
