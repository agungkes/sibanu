import { FONT_MEDIUM, FONT_REGULAR } from './fonts';
import { Theme } from 'react-native-elements';
import { height, width } from './helpers';

const theme: Theme = {
  colors: {
    primary: '#FE502F',
    white: '#F2F2F2',
    platform: {
      default: {
        primary: '#FE502F',
        grey: '#333333',
        secondary: '#211046',
      },
    },
  },

  Button: {
    buttonStyle: {
      height: height(58),
      borderWidth: width(1.5),
      // backgroundColor: 'red',
      // borderColor: '#FE502F',
    },
    titleStyle: {
      textTransform: 'capitalize',
      fontFamily: FONT_MEDIUM,
      fontSize: height(18),
    },
    containerStyle: {
      marginBottom: height(16),
    },
  },
  Text: {
    style: {
      fontFamily: FONT_REGULAR,
      fontSize: height(14),
    },
  },

  Input: {
    containerStyle: {
      paddingHorizontal: 0,
    },
    placeholderTextColor: '#DDDDDD',
    inputContainerStyle: {
      borderWidth: width(1.5),
      backgroundColor: '#fff',
      borderRadius: width(5),
      borderColor: '#DDDDDD',
    },
    inputStyle: {
      borderBottomWidth: 0,
      height: height(56),
      paddingHorizontal: width(16),
    },
    labelStyle: {
      color: '#333333',
      fontSize: height(20),
      fontFamily: FONT_MEDIUM,
      marginBottom: height(16),
    },
    rightIconContainerStyle: {
      paddingRight: width(16),
    },
  },

  CheckBox: {
    containerStyle: {
      backgroundColor: 'transparent',
      borderWidth: 0,
    },
  },
};

export default theme;
