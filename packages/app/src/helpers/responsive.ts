import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const DESIGN_WIDTH = 375;
const DESIGN_HEIGHT = 812;

const width = (number: number) => {
  return wp((number / DESIGN_WIDTH) * 100);
};

const height = (number: number) => {
  return hp((number / DESIGN_HEIGHT) * 100);
};

export { width, height };
