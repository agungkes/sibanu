const generateTitle = (title: string) => {
  const avatarTitle = title.split(' ');

  if (avatarTitle.length === 1) {
    return avatarTitle[1];
  }

  return `${avatarTitle[0][0]}${avatarTitle[1][0]}`;
};
export default generateTitle;
