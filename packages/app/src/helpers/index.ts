export * from './responsive';
import generateTitle from './generateTitle';
import generateColor from './generateColor';

export { generateTitle, generateColor };
