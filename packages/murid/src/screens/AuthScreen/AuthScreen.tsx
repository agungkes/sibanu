import { Authentication } from '@sibanu/app';
import React from 'react';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

type Props = NativeStackScreenProps<RootStackParamList, 'AUTH_HOME'>;

const AuthScreen = ({ navigation }: Props) => {
  const onRegister = () => {
    navigation.navigate('REGISTER');
  };

  const onSignIn = () => {
    navigation.navigate('LOGIN');
  };
  return (
    <Authentication
      title="peserta didik"
      onRegister={onRegister}
      onSignIn={onSignIn}
    />
  );
};

export default AuthScreen;
