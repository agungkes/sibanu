import 'react-native-screens';

import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { ThemeProvider as RNEProvider } from '@sibanu/app';
import HomeScreen from './src/screens/HomeScreen';
import { theme, FONT_SEMIBOLD } from '@sibanu/app';
import routes from '@murid/config/routes';
import AuthScreen, {
  RegisterScreen,
  LoginScreen,
} from '@murid/screens/AuthScreen';

const Stack = createNativeStackNavigator();

const Navigator = () => {
  return (
    <RNEProvider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName={routes.AUTHENTICATION.HOME}
          screenOptions={{
            headerTintColor: theme.colors?.primary,
            headerTitleAlign: 'center',
            headerTitleStyle: {
              color: '#000',
              fontSize: 18,
              fontFamily: FONT_SEMIBOLD,
            },
          }}>
          <Stack.Group>
            <Stack.Screen
              name={routes.AUTHENTICATION.HOME}
              component={AuthScreen}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name={routes.AUTHENTICATION.REGISTER}
              component={RegisterScreen}
              options={{
                headerTitle: 'Register',
              }}
            />
            <Stack.Screen
              name={routes.AUTHENTICATION.LOGIN}
              component={LoginScreen}
              options={{
                headerTitle: 'Sign In',
              }}
            />
          </Stack.Group>
          <Stack.Screen name="Home" component={HomeScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </RNEProvider>
  );
};

AppRegistry.registerComponent(appName, () => Navigator);
